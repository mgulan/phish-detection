from easydict import EasyDict as edict

_C = edict()

_C.DATASET = edict()
_C.DATASET.TRAIN = 'dataset/images'

_C.MODEL = edict()
_C.MODEL.PRETRAINED = True

_C.INPUT = edict()
_C.INPUT.SIZE = (224, 224)

_C.TRAIN = edict()
_C.TRAIN.BATCH_SIZE = 2
_C.TRAIN.SHUFFLE = True
_C.TRAIN.LR = 1e-5
_C.TRAIN.NUM_EPOCHS = 50
_C.TRAIN.CHECKPOINT_DIR = 'experiments'