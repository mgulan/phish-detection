import torch
from model import TripletNetwork
from utils import load_checkpoint

from pathlib import Path
from PIL import Image
import torchvision.transforms as transforms

model = TripletNetwork()
load_checkpoint('model.pth', model)
model.eval()

sites = [x.name for x in Path('dataset/images').iterdir() if x.is_dir()]

transform = transforms.Compose([
            transforms.Resize((224, 224)),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                  std=[0.229, 0.224, 0.225])
])

features = []
for site in sites:
    path = Path('dataset/images') / site
    
    images = []
    for image in path.iterdir():
        with open(image, "rb") as f:
            img = Image.open(f).convert("RGB")
            img = transform(img)
            images.append(img)
    
    feature = model.get_features(torch.stack(images))
    feature = torch.mean(feature, dim = 0)
    features.append(feature)

torch.save(torch.stack(features), 'features.pth')