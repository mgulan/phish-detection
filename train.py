import os
from tqdm import tqdm

from config import _C as cfg
from model import TripletNetwork
from data import PhishDataset, build_data_loader
from utils import AverageMeter, save_checkpoint

import torch.optim as optim

def step(model, optimizer, data_loader):
    losses = AverageMeter('Loss', ':.4f')
    model.train()

    with tqdm(total = len(data_loader)) as pbar:
        for i, x in enumerate(data_loader):
            loss = model(x)

            optimizer.zero_grad()
            loss.backward()

            optimizer.step()

            losses.update(loss.item(), x[0].size(0))

            pbar.set_postfix(loss = '{:05.3f}'.format(loss.item()))
            pbar.update()

    print(losses)
    # TODO logging

def train(model, optimizer, data_loader, num_epochs = 50):

    for epoch in range(num_epochs):
        print(f'Epoch {epoch + 1} / {num_epochs}:')
        step(model, optimizer, data_loader)
        
        checkpoint_path = os.path.join(cfg.TRAIN.CHECKPOINT_DIR, f'model_{epoch:05d}.pth')
        save_checkpoint({
            'epoch': epoch + 1,
            'state_dict': model.state_dict(),
            'optim_dict': optimizer.state_dict()
        },  checkpoint_path)

if __name__ == '__main__':
    print('Config:', cfg)
    model = TripletNetwork()
    optimizer = optim.Adam(model.parameters(), lr = cfg.TRAIN.LR)
    data_loader = build_data_loader(cfg)

    train(model, optimizer, data_loader, cfg.TRAIN.NUM_EPOCHS)
