import argparse

from model import TripletNetwork
from utils import load_checkpoint

from pathlib import Path
from PIL import Image

import torch
import torchvision.transforms as transforms

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Evaluate Phish Detector")
    parser.add_argument(
        "--image",
        metavar="FILE",
        help="path to image file",
    )
    parser.add_argument(
        "--model",
        default="model.pth",
        metavar="FILE",
        help="path to model",
    )
    parser.add_argument(
        "--features",
        default="features.pth",
        metavar="FILE",
        help="path to features",
    )
    parser.add_argument(
        "--threshold",
        default = 0.9        
    )
    args = parser.parse_args()

    model = TripletNetwork()
    load_checkpoint(args.model, model)
    model.eval()

    sites = ['bbc.com', 'cnn.com', 'njuskalo.hr', 'google.com', 'amazon.com', 'theguardian.com', 'ebay.com', 'youtube.com', 'github.com', 'spiegel.de']

    transform = transforms.Compose([
            transforms.Resize((224, 224)),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                  std=[0.229, 0.224, 0.225])
    ])

    image_file = Path(args.image)
    img = Image.open(image_file).convert("RGB")
    img = transform(img)
    feat = model.get_features(img.unsqueeze(0))

    dist = torch.norm(torch.load(args.features) - feat, dim = 1)
    idx = torch.argmin(dist)
    if dist[idx] < args.threshold: 
        print(f'Site: {sites[idx]}')
    else:
        print('Unknown site')
