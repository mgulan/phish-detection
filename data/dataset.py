import random

from typing import Dict, List
from pathlib import Path
from PIL import Image

import torch
import torch.utils.data as data

class PhishDataset(data.Dataset):

    def __init__(self, path: str, transforms) -> None:
        self.dataset = self.__create_dataset(path)
        self.pages = list(self.dataset.keys())
        self.path = path
        self.transform = transforms
    
    def __getitem__(self, index: int) -> List[torch.Tensor]:
        page = self.pages[index]

        ai = random.randint(0, len(self.dataset[page]) - 1)
        a = self.load_image(page, ai)
        a = self.transform(a)

        pi = random.randint(0, len(self.dataset[page]) - 1)
        while pi == ai:
            pi = random.randint(0, len(self.dataset[page]) - 1)
        
        p = self.load_image(page, pi)
        p = self.transform(p)

        npage = random.choice(self.pages)
        while npage == page:
            npage = random.choice(self.pages)
        
        ni = random.randint(0, len(self.dataset[npage]) - 1)

        n = self.load_image(npage, ni)
        n = self.transform(n)

        return [a, p, n]

    def __create_dataset(self, path: str) -> Dict[str, List[str]]:
        dataset = {}
        for file in Path(path).glob('*/*.png'):
            file_name = file.name
            folder_name = file.parent.name
            if folder_name in dataset:
                dataset[folder_name].append(file_name)
            else:
                dataset[folder_name] = [file_name]
        return dataset

    def __len__(self) -> int:
        return len(self.dataset)

    def load_image(self, page, index) -> Image.Image:
        image_path = Path(self.path) / page / self.dataset[page][index]
        with open(image_path, "rb") as f:
            img = Image.open(f)
            return img.convert("RGB")
