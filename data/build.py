import random
import time

import numpy as np

import torch
import torch.utils.data as torchdata
import torchvision.transforms as transforms

from .dataset import PhishDataset

def build_data_loader(cfg):
    dataset = PhishDataset(cfg.DATASET.TRAIN, transforms.Compose([
            transforms.Resize(cfg.INPUT.SIZE),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                  std=[0.229, 0.224, 0.225])
    ]))

    return torchdata.DataLoader(
        dataset,
        batch_size = cfg.TRAIN.BATCH_SIZE,
        shuffle = cfg.TRAIN.SHUFFLE,
        drop_last = True,
        num_workers = 0
    )

def set_seed(seed = None):
    if seed is None:
        seed = int(time.time())
    random.seed(seed)
    torch.manual_seed(seed)
    np.random.seed(seed)
