import torch

from pathlib import Path
class AverageMeter(object):
    """
    Computes and stores the average and current value

    Code copied from https://github.com/pytorch/examples/blob/master/imagenet/main.py
    """
    def __init__(self, name, fmt=':f'):
        self.name = name
        self.fmt = fmt
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

    def __str__(self):
        fmtstr = '{name} {val' + self.fmt + '} ({avg' + self.fmt + '})'
        return fmtstr.format(**self.__dict__)

def save_checkpoint(state, checkpoint):
    Path(checkpoint).parent.mkdir(parents=True, exist_ok=True)
    torch.save(state, checkpoint)

def load_checkpoint(checkpoint, model):
    checkpoint = torch.load(checkpoint)
    model.load_state_dict(checkpoint['state_dict'])