# Phish Detection

## Model
Implementiran je model *Triplet Network* inspiriran modelom korištenim u radu [FaceNet: A Unified Embedding for Face Recognition and Clustering](https://arxiv.org/pdf/1503.03832.pdf). Model se sastoji od podmodela [VGG16](https://arxiv.org/pdf/1409.1556.pdf) kojemu je zadnji sloj zamijenjen slojem sa $`128`$ značajki. U svakoj iteraciji treniranja modela, u model se dovode tri slike: `anchor`, `positive` i `negative`. Cilj modela je postići što manju razliku između značajki `anchor`-a i `positive`-a te što veću razliku između `anchor`-a i `negative`-a. U tu svrhu koristi se funkcija gubitka poznata pod nazivom `triplet loss`, a dana je izrazom:

```math
\mathcal{L}(A, P, N) = max(|| f(A) -f(P)||^2-||f(A) -f(N)||^2 + \alpha, 0)
```

Pri tome, $`A`$ predstavlja `anchor` sliku, $`P`$ `positive` te $`N`$ `negative` sliku. Funkcija $`f`$ predstavlja model, dok $`\alpha`$ predstavlja marginu između pozitivnih i negativnih parova. 

Model je implementiran u datoteci *[model\model.py](model\model.py)*.

## Dataset
Dataset je organiziran na sljedeći način:

```
(root) /
  site.com/
    img_000.png
    img_001.png
    ...
  site.hr/
    img_000.png
    img_001.png
    ...
```

U root folderu dataseta se treba nalaziti $`N`$ podfoldera, pri čemu $`N`$ označava broj web-stranica koje model treba prepoznati. Svaki podfolder mora nositi naziv kao FQDN web-stranice koju predstavlja. U svakom od podfoldera se treba nalaziti proizvoljan broj *screenshot*-ova web-stranice koju podfolder predstavlja.

Kod za učitavanje dataseta dan je u datoteci *[data\dataset.py](data\dataset.py)*.

## Treniranje modela

Kod za treniranje modela prikazan je u datoteci *[train.py](train.py)*. U kodu je implementiran postupak treniranja modela s konfigurabilnim brojem epoha, veličinom mini-grupe, stopom učenja i drugim hiperparametrima. Hiperparametre je moguće podesiti u datoteci *[config.py](config.py)*. 

Nakon svake epohe stanje modela se sprema u datoteku te ga je moguće poslije upotrijebiti za evaluaciju.


## Evaluacija modela

Nakon što se model nauči, uz pomoć datoteke *[get_features.py](get_features.py)* moguće je dobiti značajke modela za svaku naučenu web-stranicu.

Dobivene značajke se proslijeđuju kao jedan od parametara prilikom pokretanja datoteke *[evaluate.py](evaluate.py)*. Uz značajke, potrebno je proslijediti i datoteku sa stanjem modela te *screenshot* web-stranice za koju želimo evaluirati model. Program bi trebao ispisati FQDN ako model prepoznaje web-stranicu, `Unknown site` inače.
