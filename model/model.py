from typing import List

import torch
import torch.nn as nn
import torch.nn.functional as F

import torchvision.models as models

__all__ = ['TripletNetwork']

class TripletNetwork(nn.Module):
    def __init__(self) -> None:
        super(TripletNetwork, self).__init__()
        self.vgg16 = self.__build_vgg16()
        
    def forward(self, x: List[torch.Tensor]) -> torch.Tensor:
        a, p, n = x
        
        a = self.vgg16(a)
        a = F.normalize(a, dim = -1, p = 2) # L2 normalization
        
        p = self.vgg16(p)
        p = F.normalize(p, dim = -1, p = 2) # L2 normalization
        
        n = self.vgg16(n)
        n = F.normalize(n, dim = -1, p = 2) # L2 normalization
        
        return self.loss_function(a, p, n)

    def __build_vgg16(self) -> nn.Module:
        vgg16 = models.vgg16(pretrained = True)
        vgg16.classifier[-1] = nn.Linear(4096, 128, bias = False)
        return vgg16

    def loss_function(self, a, p, n, alpha = 0.2):
        pd = torch.sum(torch.square(a - p), axis = 1)
        nd = torch.sum(torch.square(a - n), axis = 1)
        loss = pd - nd + alpha
        loss = torch.mean(torch.clamp(loss, min=0.0))
        return loss

    def get_features(self, x):
        return F.normalize(self.vgg16(x), dim = -1, p = 2)